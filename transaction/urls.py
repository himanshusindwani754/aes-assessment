from django.urls import path, include
from . import views
from .views import *

urlpatterns = [
	path('api/transaction/', TransactionView.as_view()),
	path('api/transaction-list/<int:pk>', TransactionListView.as_view()),
	path('api/transaction-line/', TransactionLineView.as_view()),
	path('api/inventory-items/', InventoryItemsView.as_view()),
	path('api/transaction-line-items/<int:pk>', TransactionLineItemsView.as_view()),
	path('api/delete-transaction/<int:pk>', DeleteTransactionView.as_view())
]