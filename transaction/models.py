from django.db import models
from datetime import date

todays_date = date.today()
   

def increment_invoice_number():
    last_invoice = Transaction.objects.all().order_by('id').last()
    if not last_invoice:
        return 'TRN/1/2021'
    else:
        invoice_no = last_invoice.transaction_number
        invoice_no_split = invoice_no.split('/')
        invoice_year = int(invoice_no_split[2])
        if str(todays_date.year) == str(invoice_year):
            return invoice_no
        else:
            return 'TRN/1/' + str(todays_date.year)
    invoice_no = last_invoice.transaction_number
    invoice_no_split = invoice_no.split('/')
    invoice_int = int(invoice_no_split[1])
    new_invoice_int = invoice_int + 1
    new_invoice_no = 'TRN/' + str(new_invoice_int) +'/'+ str(todays_date.year)
    return new_invoice_no  


# Masters required in transaction models
class BranchMaster(models.Model):
    short_name = models.CharField(max_length=10, unique=True)
    contact_person_name = models.CharField(max_length=20)
    gst_number = models.CharField(max_length=20)
    address1 = models.CharField(max_length=50)
    pin_code = models.CharField(max_length=10)
    mobile = models.CharField(blank=True, null=True, max_length=10)


class DepartmentMaster(models.Model):
    name = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name


class CompanyLedgerMaster(models.Model):
    name = models.CharField(max_length=32, unique=True)
    gst_number = models.CharField(max_length=20, unique=True)
    supplier_status = models.BooleanField(default=False)
    address1 = models.CharField(max_length=50)
    pin_code = models.CharField(max_length=10)
    mobile = models.CharField(max_length=10)
    remarks = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.name


class ArticleMaster(models.Model):
    name = models.CharField(max_length=80, unique=True)
    short_name = models.CharField(max_length=50, unique=True)
    blend_pct = models.CharField(max_length=50)
    twists = models.PositiveIntegerField(blank=True, null=True)
    remarks = models.CharField(max_length=64, blank=True)

    def __str__(self):
        return self.name


class ColorMaster(models.Model):
    article = models.ForeignKey(ArticleMaster, on_delete=models.PROTECT)
    name = models.CharField(max_length=20)
    short_name = models.CharField(max_length=20)
    remarks = models.CharField(max_length=64, blank=True)

    def __str__(self):
        return self.name


# Create your models here.


class Transaction(models.Model):

    transaction_status_choice = (
        ('Pending', 'PENDING'), 
        ('Completed', 'COMPLETED'),
        ('Close', 'CLOSE')
        )

    company = models.ForeignKey(CompanyLedgerMaster, on_delete=models.CASCADE)
    branch = models.ForeignKey(BranchMaster, on_delete=models.CASCADE)
    department = models.ForeignKey(DepartmentMaster, on_delete=models.CASCADE)
    transaction_number = models.CharField(max_length=100,default=increment_invoice_number, null=True, blank=True)
    transaction_status = models.CharField(choices=transaction_status_choice, max_length=10)



class TransactionLine(models.Model):
    
    unit_choice = (
        ('kg', 'KG'),
        ('metre', 'METRE')
        )

    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE)
    article = models.ForeignKey(ArticleMaster, on_delete=models.CASCADE)
    colour = models.ForeignKey(ColorMaster, on_delete=models.CASCADE)
    required_on_data = models.DateTimeField()
    quantity = models.DecimalField(max_digits=10,decimal_places=0)
    rate_per_unit = models.PositiveIntegerField()
    unit = models.CharField(choices=unit_choice, max_length=10)


class InventoryItem(models.Model):

    unit_choice = (
        ('kg', 'KG'),
        ('metre', 'METRE')
        )

    article = models.ForeignKey(ArticleMaster, on_delete=models.CASCADE)
    colour = models.ForeignKey(ColorMaster, on_delete=models.CASCADE)
    company = models.ForeignKey(CompanyLedgerMaster, on_delete=models.CASCADE)
    gross_quantity = models.DecimalField(max_digits=10, decimal_places=0)
    net_quantity = models.DecimalField(max_digits=10, decimal_places=0)
    unit = models.CharField(choices=unit_choice, max_length=10)


class TransactionItems(models.Model):
    transaction_line = models.ForeignKey(TransactionLine, on_delete=models.CASCADE)
    inventory_item = models.ForeignKey(InventoryItem, on_delete=models.CASCADE)