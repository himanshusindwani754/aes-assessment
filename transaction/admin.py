from django.contrib import admin
from .models import *
# Register your models here.


admin.site.register(TransactionItems)
admin.site.register(BranchMaster)
admin.site.register(DepartmentMaster)
admin.site.register(CompanyLedgerMaster)
admin.site.register(ArticleMaster)
admin.site.register(ColorMaster)
admin.site.register(Transaction)
admin.site.register(TransactionLine)
admin.site.register(InventoryItem)
