from django.shortcuts import render
from .models import *
from rest_framework.views import APIView
from .serializers import *
from rest_framework.response import Response
from rest_framework import status


class TransactionView(APIView):
	def post(self, request):
		data=request.data
		serializer = TransactionSerializer(data=data, partial=False)
		try:
			if serializer.is_valid():
				serializer.save()
				return Response({'data':serializer.data, 'message':'Transaction has been created', 'status':200})
			else:
				return Response({'status' : 200, 'message':serializer.errors})
		except Exception as e:
			return Response({'status' : 200, 'message':str(e)})


class TransactionListView(APIView):
	def get(self, request, pk, *args,**kwargs):
		try:
			queryset = Transaction.objects.get(id=pk)
			serializer = TransactionListSerializer(queryset)
			return Response({'data':serializer.data, 'message':'Transaction List', 'status':200})
		except Exception as e:
			return Response({'status' : 200, 'message':str(e)})


class TransactionLineView(APIView):
	def post(self, request, pk, *args, **kwargs):
		transaction = Transaction.objects.get(id=pk)
		data=request.data
		serializer = TransactionLineSerializer(data=data, partial=False)
		try:
			if serializer.is_valid():
				serializer.save(transaction=transaction)
				return Response({'data':serializer.data, 'message':'Transaction has been created', 'status':200})
			else:
				return Response({'status' : 200, 'message':serializer.errors})
		except Exception as e:
			return Response({'status' : 200, 'message':str(e)})


class InventoryItemsView(APIView):
	def post(self, request, *args, **kwargs):
		data=request.data
		serializer = InventoryItemsSerializer(data=data, partial=True, many=True)
		try:
			if serializer.is_valid():
				serializer.save()
				return Response({'data':serializer.data, 'message':'Data has been added', 'status':200})
			else:
				return Response({'status' : 200, 'message':serializer.errors})
		except Exception as e:
			return Response({'status' : 200, 'message':str(e)})


class TransactionLineItemsView(APIView):
	def post(self, request, pk, *args, **kwargs):
		data=request.data
		transaction_line = TransactionLine.objects.get(id=pk)
		serializer = TransactionLineItemsSerializer(data=data, partial=True, many=True)
		try:
			if serializer.is_valid():
				serializer.save(transaction_line=transaction_line)
				return Response({'data':serializer.data, 'message':'Data has been added', 'status':200})
			else:
				return Response({'status' : 200, 'message':serializer.errors})
		except Exception as e:
			return Response({'status' : 200, 'message':str(e)})

class DeleteTransactionView(APIView):
	def delete(self, request, pk, format=None):
		try:
			transaction = Transaction.objects.get(id=pk)
			transaction_line = TransactionLine.objects.get(transaction=transaction)
			
			try:
				transaction_items=TransactionItems.objects.filter(transaction_line=transaction_line)
			except:
				return None

			if not transaction_items:
				transaction.delete()
				return Response({'status' : 200, 'message':'Transaction has been deleted'})

			else:
				return Response({'status' : 200, 'message':'Transaction can not be deleted as it has some items'})

		except Exception as e:
			return Response({'status' : 200, 'message':str(e)})
