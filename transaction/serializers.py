from rest_framework import serializers
from .models import *
from rest_framework_bulk import (
    BulkListSerializer,
    BulkSerializerMixin,
    ListBulkCreateUpdateDestroyAPIView,
)


class TransactionSerializer(serializers.ModelSerializer):

	class Meta:
		model = Transaction
		fields = '__all__'


class TransactionLineSerializer(serializers.ModelSerializer):

	class Meta:
		model = TransactionLine


class InventoryItemsSerializer(serializers.ModelSerializer):
	class Meta:
		model = InventoryItem
		fields = '__all__'


class TransactionLineItemsSerializer(serializers.ModelSerializer):
	class Meta:
		model = TransactionItems
		fields = ('inventory_item',)


class TransactionItemsListSerializer(serializers.ModelSerializer):

	article = serializers.CharField(source='inventory_item.article')
	colour = serializers.CharField(source='inventory_item.colour')
	gross_quantity = serializers.CharField(source='inventory_item.gross_quantity')
	net_quantity = serializers.CharField(source='inventory_item.net_quantity')
	unit = serializers.CharField(source='inventory_item.unit')


	class Meta:
		model = TransactionItems
		fields = ('transaction_line', 'article', 'colour', 'gross_quantity', 'net_quantity','unit')


class TransactionListSerializer(serializers.ModelSerializer):

	inventory_items = serializers.SerializerMethodField()

	class Meta:
		model = Transaction
		fields = ('company', 'branch', 'department', 'transaction_number', 'transaction_status', 'inventory_items')

	def get_inventory_items(self, obj):
		transaction_line = TransactionLine.objects.get(transaction=obj)
		inventory_items = TransactionItems.objects.filter(transaction_line=transaction_line)
		serializers_data = TransactionItemsListSerializer(inventory_items, many=True)
		return serializers_data.data


